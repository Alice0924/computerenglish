# ComputerEnglish  期末考核作业


### 作业内容

请完成后[Operating System Concepts](Abraham-Silberschatz-Operating-System-Concepts-10th-2018.pdf)一书中Chapter1-10中每章节课后题。

### 作业要求

按照书目章节题号回答，作答在word文档中，命名为`学号-姓名.docx`，例如`20180001-张三.docx`，通过git上传到本仓库中。上传需新建分支，分支名为姓名全拼，例如`zhangsan`。

### 截止时间

截止时间为考试周结束后一周后